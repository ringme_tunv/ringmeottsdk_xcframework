#import "CarbonTabSwipeNavigation.h"
#import "SVPullToRefresh.h"
#import "grdb_config.h"
#import "CallSDK.h"
#import <RingmeReactSDK/RingmeReactSDK-Swift.h>

/**
 Hướng dẫn build RingmeOttSDK - SDK cho tính năng chat
 B1: Kéo thả lần lượt 3 thư viện: "RingmeReactSDK", "RingmeXMPPFramework", "WebRTC" vào dự án
 B2: Kéo source code RingmeOTT vào dự án
 B3: Chuột phải vào "RingmeReactSDK" sau khi đã kéo vào dự án -> chọn Show in finder -> ios-arm64 -> Header -> Chọn 2 file "CallSDK.h" và "RingmeReactSDK-swift.h" và kéo vào dự án
 B4: Chọn Scheme RingmeOttSdkUniversal và build
 B5: Sử dụng file kết quả là "RingmeOttSdk.xcframework"
 
 Thông tin các thư viện
 RingmeReactSDK: https://repos.ringme.vn/releases/ios/call-sdk-ios/1.1.10/RingmeReactSDK.xcframework.zip
 RingmeXMPPFramework: https://repos.ringme.vn/releases/ios/xmpp-core-ios/1.1.5/RingmeXMPPFramework.xcframework.zip
 WebRTC:   pod 'JitsiWebRTC', '111.0.2'
 
 */
